import sys
import requests
import lxml.html as lh
import pandas as pd
import argparse
import datetime
import dateparser

def taiwan_bank( currency, qdate, style):
    url='https://rate.bot.com.tw/xrt/quote/' + qdate.isoformat() + '/'+ currency +'/spot'
    page = requests.get(url)
    doc = lh.fromstring(page.content)
    tr_elements=doc.xpath('//tr')
    col = []
    date = tr_elements[2][0].text_content().split()[0]

    if date == "很抱歉，本次查詢找不到任何一筆資料！":
        print("%s 尚未有資料" % qdate)
        return
    
    hiRate, lowRate = float(tr_elements[2][5].text_content()), float(tr_elements[2][5].text_content())
    
    i = 0
    for t in tr_elements[0]:
        i = i+1
        name = t.text_content()
        #print('%d:"%s"'%(i,name))
        col.append((name,[]))


    for j in range(2, len(tr_elements)):
        tds = tr_elements[j]
        time = tds[0].text_content().split()[1]
        cur = tds[1].text_content()
        # 現金匯率 本行買入/賣出 & 即期匯率 買入/賣出 
        cash_buyin  = float(tds[2].text_content())
        cash_buyout = float(tds[3].text_content())
        real_buyin  = float(tds[4].text_content())
        real_buyout = float(tds[5].text_content())
        if real_buyout > hiRate:
            hiRate = real_buyout
        if real_buyout < lowRate:
            lowRate = real_buyout

        if style == 1:
            # Complicated Output(Hourly)
            print("%s, %s, %s, %f, %f, %f, %f" % 
                    (date, time, currency, cash_buyin, cash_buyout, real_buyin, real_buyout))
    if style == 2:
        # Simple Output(daily)
        print("%s 最高: %f, 最低: %f" % (qdate, hiRate, lowRate))


if __name__ == '__main__':
    today = datetime.datetime.now()
    todate = today.date()
    weekago = (today - datetime.timedelta(days=7)).date()
    parser = argparse.ArgumentParser(prog='CurrencyRateTest', description="test")
    parser.add_argument('--bank', '-b', type=str, default='t', help="default: t for taiwan bank")
    parser.add_argument('--datefrom', '-f', type=str, default=weekago.isoformat(), help="date like 2019-05-10")
    parser.add_argument('--datetill', '-t', type=str, default=todate.isoformat(), help="date like 2019/05/10")
    parser.add_argument('--currency', '-c', type=str, default="jpy", help="like jpy or usd")
    parser.add_argument('--style', '-s', type=int, default=1, help="style 1=印出全部, 2=當日最高最低")
    parsed = parser.parse_args()

    d1 = dateparser.parse( parsed.datefrom ).date()
    d2 = dateparser.parse( parsed.datetill ).date()
    delta = d2 - d1
    if d2 < d1:
        print("date wrong")
        sys.exit()

    cur = parsed.currency.upper()
    
    while d1 <= d2:
        if parsed.bank == 't':
            taiwan_bank( cur, d1, parsed.style)
        d1 = d1 + datetime.timedelta(days=1)


